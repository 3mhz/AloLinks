jQuery(document).ready(function($) {
	var defaultGroup = "icons";
	
	var groupList = [
		{
			"label" : "Icons",
			"value" : "icons",
			"urls" : [
				{
					"name" : "Flat Icon",
					"host" : "http://www.flaticon.com/packs"
				},
				{
					"name" : "Flat Icons",
					"host" : "http://www.flaticons.net"
				},
				{
					"name" : "Font Awesome",
					"host" : "http://fortawesome.github.io/Font-Awesome/"
				},
				{
					"name" : "Icon Monstr",
					"host" : "http://iconmonstr.com/"
				}
			]
		},
		{
			"label" : "Images",
			"value" : "images",
			"urls" : [
				{
					"name" : "The Stocks",
					"host" : "http://www.thestocks.im"
				},
				{
					"name" : "IM Creator",
					"host" : "http://www.imcreator.com/free"
				},
				{
					"name" : "Pixabay",
					"host" : "http://pixabay.com/en/",
					"target" : "_blank"
				}
			]
		},
		{
			"label" : "Colors",
			"value" : "colors",
			"urls" : [
				{
					"name" : "Flat UI Color Picker",
					"host" : "http://www.flatuicolorpicker.com/"
				},
				{
					"name" : "Flat UI Colors",
					"host" : "http://flatuicolors.com/"
				},
				{
					"name" : "Bootflat",
					"host" : "http://bootflat.github.io/documentation.html"
				}
			]
		},
		{
			"label" : "Fonts",
			"value" : "fonts",
			"urls" : [
				{
					"name" : "Fonts",
					"host" : "http://www.fonts.com/"
				},
				{
					"name" : "Font Squirrel",
					"host" : "http://www.fontsquirrel.com/"
				},
			]
		},
		{
			"label" : "Bootstrap Tools",
			"value" : "bootstrap-tools",
			"urls" : [
				{
					"name" : "Twitter Bootstrap",
					"host" : "http://getbootstrap.com/getting-started/"
				},
				{
					"name" : "Boot Bundle",
					"host" : "http://www.bootbundle.com/"
				},
				{
					"name" : "Bootsnipp",
					"host" : "http://www.bootsnipp.com/"
				}
			]
		},
		{
			"label" : "Magazines and Talks",
			"value" : "magazines-and-talks",
			"urls" : [
				{
					"name" : "Smashing Magazine",
					"host" : "http://www.smashingmagazine.com/"
				},
				{
					"name" : "Tech Crunch",
					"host" : "http://www.techcrunch.com/"
				},
				{
					"name" : "Ted Talk",
					"host" : "http://www.ted.com/",
					"target" : "_blank"
				}
			]
		},
		{
			"label" : "Free Designs",
			"value" : "free-designs",
			"urls" : [
				{
					"name" : "CSS Author",
					"host" : "http://www.cssauthor.com/",
					"favicon" : "http://www.cssauthor.com/wp-content/uploads/2014/09/favicon.ico"
				},
				{
					"name" : "Web Designer Hub",
					"host" : "http://www.webdesignerhub.com/"
				},
				{
					"name" : "Freebies Bug",
					"host" : "http://freebiesbug.com/"
				},
				{
					"name" : "Pixeden",
					"host" : "http://www.pixeden.com/",
					"favicon" : "http://www.pixeden.com/templates/pixeden/images/favicon.ico"
				},
				{
					"name" : "Codrops",
					"host" : "http://tympanus.net/codrops"
				},
				{
					"name" : "Abduzeedo",
					"host" : "http://abduzeedo.com/"
				},
				{
					"name" : "1st Web Designer",
					"host" : "http://www.1stwebdesigner.com/"
				},
				{
					"name" : "Flat Design.IO",
					"host" : "http://flatdesign.io/"
				}
			]
		},
		{
			"label" : "Inpirations",
			"value" : "inspirations",
			"urls" : [		
				{
					"name" : "Awwwards",
					"host" : "http://www.awwwards.com/"
				},
				{
					"name" : "Flat Inspiration",
					"host" : "http://www.flatinspire.com/"
				},
				{
					"name" : "Behance",
					"host" : "http://www.behance.net/"
				},
				{
					"name" : "Dribbble",
					"host" : "http://www.dribbble.com/",
					"target" : "_blank"
				}
			]
		},
		{
			"label" : "Coding",
			"value" : "coding",
			"urls" : [		
				{
					"name" : "Laravel Docs",
					"host" : "http://laravel.com/docs"
				},
				{
					"name" : "Cake PHP API",
					"host" : "http://api.cakephp.org/"
				}
			]
		}
	];
	

	$.each(groupList, function(i) {
		var group = groupList[i];
		$("#aloGroup").append('<option value="'+group.value+'">'+group.label+'</option>');
		setGroup(group);
	});
	var $_groupLiTags = $("#groupUrls li");
	/*function getGroup(groupObj) {
		var selectedGroup = false;
		$.each(groupList, function(i) {
			var group = groupList[i];
			if(group.value == groupObj.value) {
				selectedGroup = group;
			}
		});
		return selectedGroup;
	}*/


	$("#aloGroup").change(function() {
		showGroup($(this).val());
	});

	$("#aloGroup").val(defaultGroup);
	showGroup(defaultGroup);

	function showGroup(groupCode) {
		$_groupLiTags.hide();
		$("#groupUrls li.group-"+groupCode).show();
		var index = Math.floor(Math.random() * $("#groupUrls li.group-"+groupCode).length);
		var liTag = $("#groupUrls li.group-"+groupCode+":eq("+index+")");
		var loadUrl = liTag.find("a[data-href]").attr('data-href');
		setFrameHost(loadUrl);
		setActiveClass(liTag);
	}

	function setGroup(group) {
		var groupCode = group.value;
		if(group.urls !== undefined) {
			var loadUrl = '#';
			var ulTag = $("#groupUrls");
			for(var i = 0; i < group.urls.length; i++) {
				if(i == 0) {
					loadUrl = group.urls[i].host;
				}
				var groupUrl = group.urls[i];
				var liTag = $('<li>');
				liTag.addClass('group-'+groupCode);
				var aTag = $('<a>');
				aTag.attr('href', '#');
				var aTagHTML = '';
				if(groupUrl.favicon !== undefined)
				{
					var imgTag = $("<img>");
					imgTag.attr('src', groupUrl.favicon);
					imgTag.attr('class', 'pull-left');
					//aTagHTML += "<img src='"+groupUrl.favicon+"' class='pull-left' />";
				}
				aTagHTML += groupUrl.name;
				if(groupUrl.target !== undefined)
				{
					aTag.attr('target', groupUrl.target);
					if(groupUrl.target == '_blank')
					{
						aTag.attr('href', groupUrl.host);
						aTagHTML += '<span class="glyphicon glyphicon-new-window pull-right"></span>';
					}
				} else {
					//aTag.attr('target', 'aloWebFrame');					
					aTag.attr('data-href', groupUrl.host);
					aTag.on('click', setClickUrlEvent);
				}
				aTag.html(aTagHTML);
				liTag.append(aTag);
				ulTag.append(liTag);
			}
		}
	}

	//$("#groupUrls > li > a").on('click', setClickUrlEvent);

	function setClickUrlEvent() {
		var loadUrl = $(this).attr('data-href');
		setFrameHost(loadUrl);
		$("#groupUrls > li").removeClass('active');
		$(this).parent().addClass('active');	
	}
	
	//setFrameHost(getGroup(defaultGroup).urls[0].host);

	function setFrameHost(loadUrl) {
		$("#aloWebFrame").attr("src", loadUrl);
	}

	function setActiveClass(liTag) {
		$_groupLiTags.removeClass('active');
		liTag.addClass('active');
	}
	
});